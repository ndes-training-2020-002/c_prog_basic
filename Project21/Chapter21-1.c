#include <stdio.h>    

float result1(float, float);             // tax_priceにcを返して計算する関数
int result2(float, float, float*);       // 引数としてtax_priceを渡す関数
int result3(float, float);               // メンバ変数のtax_priceに計算結果を入れる関数

float tax_price;

int main()
{          // コメント
	float price, tax;
	printf("金額に0を入力すると終了します\n");
	while (1) {                                           // 無限ループ
		printf("\n金額を入力してください = ");
		scanf_s("%f" , &price);
		if (price == 0.0) {                               // ループを抜け出す処理
			break;
		}
		printf("消費税率を入力してください(%%) = ");
		scanf_s("%f" , &tax);

		tax_price = result1(price, tax);
		printf("%-6.0f円に%-2.0f％の消費税を加えると%-6.0f円になる\n", price, tax, tax_price);

		result2(price, tax , &tax_price);
		printf("%-6.0f円に%-2.0f％の消費税を加えると%-6.0f円になる\n", price, tax, tax_price);

		result3(price, tax);
		printf("%-6.0f円に%-2.0f％の消費税を加えると%-6.0f円になる\n", price, tax, tax_price);
	}
	return 0;
}

float result1(float a, float b)
{
	float c;
	c = a * (1 + b / 100);
	return c;
}

int result2(float a, float b, float *c)
{
	*c = a * (1 + b / 100);
	return 0;
}

int result3(float a, float b)
{
	tax_price = a * (1 + b / 100);
	return 0;
}